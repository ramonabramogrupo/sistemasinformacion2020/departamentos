<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear nuevo empleado', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'], // NUMERO DE ORDEN

            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            'salario',
            'comision',
            'dept_no',

            // ['class' => 'yii\grid\ActionColumn'], // estos son los botones que vienen de serie
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {listar}',
            'buttons' => [
                'update' => function ($url,$model) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-user"></span>', 
                        $url);
                },
                'listar' => function ($url,$model,$key) {
                    return Html::a('<span class="glyphicon glyphicon-th-list"></span>', $url);
                },
	        ],
            ],
        ],
    ]); ?>


</div>
